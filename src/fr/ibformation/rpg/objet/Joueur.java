package fr.ibformation.rpg.objet;

public class Joueur {
	private int pointVie;
//	private boolean vivant;
	
	public Joueur() {
		this.pointVie = 150;
//		this.vivant = true;		// Attribut calcul�
	}

//	public Joueur(int pointVie) {
//		super();
//		this.pointVie = pointVie;
//		this.vivant = true;
//	}

	public int getPointVie() {
		return pointVie;
	}

	public void setPointVie(int pointVie) {
		if(this.pointVie < 0) {
			this.pointVie = 0;
			return;
		}
		this.pointVie = pointVie;
	}

	public boolean isVivant() {
		if (getPointVie() > 0) {
			return true;
		} else {
			return false;
		}
	}

//	public void setVivant(boolean vivant) {
//		this.vivant = vivant;
//	}

	@Override
	public String toString() {
		return "Joueur [pointVie=" + getPointVie() + ", vivant= " + isVivant() + "]";
	}
	
	public void attaque(Monstre monstre) {  
		De d6 = new De();
		if (d6.lanceLeDe() >= d6.lanceLeDe()) {
			monstre.subitDegat();
		}
	}
	
	public void attaque(Boss boss) {  
		De d25 = new De(25);
		boss.subitDegat(d25.lanceLeDe());
	}
	
	public void subitDegat(int degat) {
		De d3 = new De(3);
		if (d3.lanceLeDe() > 1) { // Bouclier 2 chance sur 6 (ou 1/3 donc d� de 3)
			this.setPointVie(pointVie = getPointVie() - degat);
		} 
	}
	

}
