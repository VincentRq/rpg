package fr.ibformation.rpg.objet;

public class Boss {
	
	private int pointVie;
	private boolean vivant;
	

	public void setPointVie(int pointVie) {
		this.pointVie = pointVie;
	}

	public boolean isVivant() {
		if (getPointVie() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void setVivant(boolean vivant) {
		this.vivant = vivant;
	}
	
	public void attaque(Joueur joueur) {
		De d25 = new De(25);
		joueur.subitDegat(d25.lanceLeDe()); 
	}
	
	public void subitDegat(int degat) {
		this.setPointVie(pointVie = getPointVie() - degat);	
	}

}
