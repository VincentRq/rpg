package fr.ibformation.rpg.objet;

public class Monstre {
	private boolean vivant;
	private final int ATTAQUE_Monstre = 10;
	private static int monstreFacileCount;
	private static int monstreDifficileCount;

	public Monstre() {
		this.vivant = true;
	}

	public Monstre(boolean vivant) {
		super();
		this.vivant = vivant;
	}

	public boolean isVivant() {
		return vivant;
	}

	public void setVivant(boolean vivant) {
		this.vivant = vivant;
	}

	public static int getMonstreFacileCount() {
		return monstreFacileCount;
	}

	public static int getMonstreDifficileCount() {
		return monstreDifficileCount;
	}

	public static void setMonstreFacileCount(int monstreFacileCount) {
		Monstre.monstreFacileCount = monstreFacileCount;
	}

	public void attaque(Joueur joueur) {
		De d6 = new De();
		if (d6.lanceLeDe() > d6.lanceLeDe()) {
			joueur.subitDegat(ATTAQUE_Monstre);
		}
	}

	public void subitDegat() {
		this.vivant = false;
		if (this instanceof MonstreFacile) {
			monstreFacileCount++;
		} else {
			monstreDifficileCount++;
		}
	}
}
