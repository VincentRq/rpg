package fr.ibformation.rpg.objet;

public class MonstreDifficile extends Monstre{

	
	
	public MonstreDifficile() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public void attaque(Joueur joueur) {
		// Attaque classique
		super.attaque(joueur);
		De d6 = new De(); 
		// Sort magique
		int deMagique = d6.lanceLeDe();
		if (deMagique != 6) {
			joueur.subitDegat(5 * deMagique);
		}
	}
	
}
