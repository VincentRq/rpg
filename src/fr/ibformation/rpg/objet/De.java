package fr.ibformation.rpg.objet;

import java.util.Random;

public class De {
	private int min;
	private int max;
	
	public De() {
		min = 1;
		max = 6;
	}
	
	public De(int max) {
		this.min = 1;
		this.max = max;
	}
	
	public De(int min, int max) {
		this.min = min;
		this.max = max;
	}
	
	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}
	
	public int lanceLeDe() {
		Random rand = new Random();
		return rand.nextInt(this.max - this.min + 1) + this.min;
	}
	
	public Monstre getMonstreRand() {
		De d2 = new De(2);
		if(d2.lanceLeDe() == 1) {
			return new MonstreFacile();
		} else {
			return new MonstreDifficile();
		}
	}
}
