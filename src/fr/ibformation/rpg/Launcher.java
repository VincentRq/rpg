package fr.ibformation.rpg;

import fr.ibformation.rpg.objet.Boss;
import fr.ibformation.rpg.objet.De;
import fr.ibformation.rpg.objet.Joueur;
import fr.ibformation.rpg.objet.Monstre;

public class Launcher {
	public static void main(String[] args) {
		Joueur j1 = new Joueur();
		De d2 = new De(2);						// Creation d'un d� pour cr�er le monstre
		Monstre monstre = d2.getMonstreRand();
		int nbPoints = Monstre.getMonstreFacileCount() + Monstre.getMonstreDifficileCount() * 2;
		Boss boss = new Boss();
		
		while (j1.isVivant() && nbPoints < 30) {
			if (monstre.isVivant()) {
				// Fight !!
				j1.attaque(monstre);
				if (monstre.isVivant()) {
					monstre.attaque(j1);
				}
			} else {
				monstre = d2.getMonstreRand();
			}
			nbPoints = Monstre.getMonstreFacileCount() + Monstre.getMonstreDifficileCount() * 2;   // MAJ de nbPoints pour boucle while
		}
		if (j1.isVivant()) {
			System.out.println("Le Boss arrive");
			j1.setPointVie(150);
			while (j1.isVivant() && boss.isVivant()) {
				j1.attaque(boss);
				if (boss.isVivant()) {
					boss.attaque(j1);
				}
				// System.out.println("J1 PV : " +  j1.getPointVie() + " ; Boss PV : " + boss.getPointVie());		// print v�rification combat
			} 
			if (j1.isVivant()) {  // print en fonction du r�sultat du combat Boss
				System.out.println("Victoire, le boss est mort");
				System.out.println(printResultPoints(Monstre.getMonstreFacileCount(), Monstre.getMonstreDifficileCount(), nbPoints));
				System.out.println("Et il te reste " + j1.getPointVie() + " points de vie");
			} else if (boss.isVivant()) {
				System.out.println("Game Over, tu es mort");
				System.out.println(printResultPoints(Monstre.getMonstreFacileCount(), Monstre.getMonstreDifficileCount(), nbPoints));
				System.out.println("Mais il  reste " + boss.getPointVie() + " points de vie au boss");
			} else {
				System.err.println("Boss et J1 tous deux vivants ?");
			}
		} else {
			// Affichage des r�sultats si pas de Boss
			System.out.println("Game Over, tu es mort");
			System.out.println(printResultPoints(Monstre.getMonstreFacileCount(), Monstre.getMonstreDifficileCount(), nbPoints));
		}
	}
	public static String printResultPoints(int monstreFacileCount, int monstreDifficileCount, int nbPoints) {
		String newLigne = System.getProperty("line.separator");
		return "Tu as tu� " + Monstre.getMonstreFacileCount() + " monstres faciles et " + Monstre.getMonstreDifficileCount() + " monstres difficiles"  + newLigne +
		"Tu as gagn� " + nbPoints + " points";
	}

}
